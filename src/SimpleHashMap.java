/**
 * @author Alexander Kachan
 */
class SimpleHashMap {
    private final float PERCENT_COLLISION = 0.3f;
    private final int DEFAULT_TABLE_SIZE = 16;
    private int countElements = 0;

    private final int KEY_NULL = 0;
    private boolean keyNullInit = false;
    private long keyNullVal;

    private int maxCollision;
    private int hashTableKey[];
    private long hashTableVal[];

    SimpleHashMap() {
        init(DEFAULT_TABLE_SIZE);
    }

    private SimpleHashMap(int hashTableSize) {
        init(hashTableSize);
    }

    private void init(int hashTableSize) {
        hashTableKey = new int[hashTableSize];
        hashTableVal = new long[hashTableSize];
        maxCollision = (int) (hashTableSize * PERCENT_COLLISION);
    }

    public int size() {
        return countElements;
    }

    public void put(int key, long val) {
        if (key == KEY_NULL) {
            keyNullVal = val;
            if (!keyNullInit) {
                keyNullInit = true;
                countElements++;
            }
            return;
        }
        putNoNull(key, val);
    }

    public long get(int key) throws NotFoundValueException {
        if (key == KEY_NULL) {
            if (keyNullInit) {
                return keyNullVal;
            } else {
                throw new NotFoundValueException("Item not found");
            }
        }
        int idHashTable = calcHashId(key);
        int countIteration = idHashTable + maxCollision;

        for (int i = idHashTable; i < countIteration; i++) {
            int id = i % hashTableKey.length;
            if (hashTableKey[id] == key) {
                return hashTableVal[id];
            }
            if (hashTableKey[id] == KEY_NULL) {
                break;
            }
        }

        throw new NotFoundValueException("Item not found");
    }

    private int calcHashId(int key) {
        return key & (hashTableKey.length - 1);
    }

    private void putNoNull(int key, long val) {
        int idHashTable = calcHashId(key);
        int countIteration = idHashTable + maxCollision;

        for (int i = idHashTable; i < countIteration; i++) {
            int id = i % hashTableKey.length;
            if (hashTableKey[id] == KEY_NULL) {
                hashTableKey[id] = key;
                hashTableVal[id] = val;
                countElements++;
                return;
            }
            if (hashTableKey[id] == key) {
                hashTableVal[id] = val;
                return;
            }
        }
        // not enough space, expanding the table and trying again
        resize();
        putNoNull(key, val);
    }

    private void resize() {
        var tmpMap = new SimpleHashMap(hashTableKey.length * 2);
        for (int i = 0; i < hashTableKey.length; i++) {
            if (hashTableKey[i] != KEY_NULL) {
                tmpMap.putNoNull(hashTableKey[i], hashTableVal[i]);
            }
        }
        hashTableKey = tmpMap.hashTableKey;
        hashTableVal = tmpMap.hashTableVal;
        maxCollision = tmpMap.maxCollision;
    }
}



