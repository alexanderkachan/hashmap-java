
import org.junit.Test;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import java.util.HashMap;


public class Tests {
    private static final int DATA_SET_SIZE = 10000;

    @Test
    public void TestPutGet() throws NotFoundValueException {
        SimpleHashMap testMap = new SimpleHashMap();
        for (int i = 0; i < DATA_SET_SIZE; i++) {
            testMap.put(i, -i);
        }
        assertEquals("size Test", testMap.size(), DATA_SET_SIZE);
        for (int i = 0; i < DATA_SET_SIZE; i++) {
            assertEquals(testMap.get(i), -i);
        }
    }

    @Test
    public void TestCollision() throws NotFoundValueException {
        SimpleHashMap testMap = new SimpleHashMap();
        final int step = 16;
        int key = 15;
        for (int i = 0; i < DATA_SET_SIZE; i++) {
            testMap.put(key, i);
            key += step;
        }

        assertEquals("size Test", testMap.size(), DATA_SET_SIZE);
        for (int i = DATA_SET_SIZE - 1; i >= 0; i--) {
            key -= step;
            assertEquals(testMap.get(key), i);
        }
    }

    @Test
    public void TestOneKey() throws NotFoundValueException {
        SimpleHashMap testMap = new SimpleHashMap();
        final int key = 7;
        for (int i = 0; i < DATA_SET_SIZE; i++) {
            testMap.put(key, -i);
        }
        assertEquals("size Test", testMap.size(), 1);
        assertEquals(testMap.get(key), -(DATA_SET_SIZE - 1));
    }

    @Test
    public void TestNegativeId() throws NotFoundValueException {
        SimpleHashMap testMap = new SimpleHashMap();

        testMap.put(-2, 5);
        assertEquals(testMap.get(-2), 5);

        testMap.put(-2, 7);
        testMap.put(-2, 3);
        assertEquals(testMap.get(-2), 3);
        assertEquals("size Test", testMap.size(), 1);
    }

    @Test
    public void TestValueNotFound() {
        SimpleHashMap testMap = new SimpleHashMap();
        try {
            testMap.get(1);
            fail("Expected an NotFoundValue to be thrown");
        } catch (NotFoundValueException e) {
        }

        try {
            testMap.get(0);
            fail("Expected an NotFoundValue to be thrown");
        } catch (NotFoundValueException e) {
        }
    }

    @Test
    public void TestNullKey() throws NotFoundValueException {
        SimpleHashMap testMap = new SimpleHashMap();
        final int key = 0;
        for (int i = 0; i < DATA_SET_SIZE; i++) {
            testMap.put(key, -i);
        }
        assertEquals("size Test", testMap.size(), 1);
        assertEquals(testMap.get(key), -(DATA_SET_SIZE - 1));
    }

    @Test
    public void RandomDataTest() throws NotFoundValueException {
        var testData = generateKayVal(DATA_SET_SIZE);
        HashMap<Integer, Long> origMap = new HashMap<>();
        SimpleHashMap testMap = new SimpleHashMap();
        for (int pairId = 0; pairId < DATA_SET_SIZE; pairId++) {
            testMap.put((int) testData[pairId][0], testData[pairId][1]);
            origMap.put((int) testData[pairId][0], testData[pairId][1]);
        }
        assertEquals("size Test", origMap.size(), testMap.size());

        for (int pairId = 0; pairId < DATA_SET_SIZE; pairId++) {
            Long mOut = testMap.get((int) testData[pairId][0]);
            Long oOut = origMap.get((int) testData[pairId][0]);
            if (oOut != null)
                assertEquals(mOut, oOut);
        }
    }

    private long[][] generateKayVal(int size) {
        final int RANDOM_MAX_VALUE = 5000;
        long[][] keyVal = new long[size][2];
        for (int i = 0; i < size; i++) {
            keyVal[i][0] = (long) (Math.random() * (RANDOM_MAX_VALUE * 2)) - RANDOM_MAX_VALUE;
            keyVal[i][1] = (long) (Math.random() * (RANDOM_MAX_VALUE * 2)) - RANDOM_MAX_VALUE;
        }
        return keyVal;
    }

}
